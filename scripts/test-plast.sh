#!/bin/bash

#/*****************************************************************************
# *                                                                           *
# *   PLAST : Parallel Local Alignment Search Tool                            *
# *   Version 2.3, released November 2015                                     *
# *   Copyright (c) 2009-2015 Inria-Cnrs-Ens                                  *
# *                                                                           *
# *   PLAST is free software; you can redistribute it and/or modify it under  *
# *   the Affero GPL ver 3 License, that is compatible with the GNU General   *
# *   Public License                                                          *
# *                                                                           *
# *   This program is distributed in the hope that it will be useful,         *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
# *   Affero GPL ver 3 License for more details.                              *
# *****************************************************************************/

# We get the directory of the script (absolute path)
_scripts_dir=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
_result=$_scripts_dir/out.txt

echo
echo "Start PLAST..."
echo

# We setup a PLAST command-line and run it to test the software
_cmdline="$_scripts_dir/../build/bin/plast -p plastp -i $_scripts_dir/../db/query.fa -d $_scripts_dir/../db/tursiops.fa -o $_result"

echo $_cmdline

eval $_cmdline

if [ ! -e $_result ] ; then
  echo "/!\ Error: PLAST fails on your system: result file does not exist: $_result"
  exit 1
fi

if [ ! -s $_result ]; then
  echo "/!\ Error: PLAST fails on your system: result file is empty: $_result"
  exit 1
fi

echo
echo "*** SUCCESS: result file is: $_result"
echo

