Author: Andreas Tille <tille@debian.org>
Last-Update: 2021-01-30
Description: Adapt test script to Debian installation
Forwarded: no
--- plast.orig/scripts/test-plast.sh
+++ plast/scripts/test-plast.sh
@@ -17,26 +17,28 @@
 # *****************************************************************************/
 
 # We get the directory of the script (absolute path)
-_scripts_dir=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
-_result=$_scripts_dir/out.txt
+BUILD_DIR=${1:-obj-$(dpkg-architecture -qDEB_HOST_GNU_TYPE)}
+DATA_DIR=$(pwd)/db
+_script=$(find "${BUILD_DIR}"/bin -name plast -executable)
+_result=$(dirname "$_script")/out.txt
 
 echo
 echo "Start PLAST..."
 echo
 
 # We setup a PLAST command-line and run it to test the software
-_cmdline="$_scripts_dir/../build/bin/plast -p plastp -i $_scripts_dir/../db/query.fa -d $_scripts_dir/../db/tursiops.fa -o $_result"
+_cmdline="$_script -p plastp -i ${DATA_DIR}/query.fa -d ${DATA_DIR}/tursiops.fa -o $_result"
 
-echo $_cmdline
+echo "$_cmdline"
 
 eval $_cmdline
 
-if [ ! -e $_result ] ; then
+if [ ! -e "$_result" ] ; then
   echo "/!\ Error: PLAST fails on your system: result file does not exist: $_result"
   exit 1
 fi
 
-if [ ! -s $_result ]; then
+if [ ! -s "$_result" ]; then
   echo "/!\ Error: PLAST fails on your system: result file is empty: $_result"
   exit 1
 fi
@@ -44,4 +46,3 @@
 echo
 echo "*** SUCCESS: result file is: $_result"
 echo
-
