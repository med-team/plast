#!/usr/bin/make -f

# DH_VERBOSE := 1

include /usr/share/dpkg/default.mk

BUILD_DIR := obj-$(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
export BDATE=$(shell TZ=UTC LC_ALL=C date -d"@$(SOURCE_DATE_EPOCH)" +'%Y-%m-%d')
export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=-lto
export DEB_CFLAGS_MAINT_APPEND += -DSIMDE_ENABLE_OPENMP -fopenmp-simd -O3
export DEB_CXXFLAGS_MAINT_APPEND += -DSIMDE_ENABLE_OPENMP -fopenmp-simd -O3

AMD64_SIMDE=sse3
I386_SIMDE=sse2 sse

%:
	dh $@ --buildsystem=cmake

override_dh_auto_configure-arch:
ifneq (,$(filter $(DEB_HOST_ARCH),amd64 i386))
	set -e ; for SIMD in $(AMD64_SIMDE) ; do \
		export CXXFLAGS="$(CXXFLAGS) -m$${SIMD}" && export CFLAGS="$(CFLAGS) -m$${SIMD}" && \
		dh_auto_configure --builddirectory=obj-$${SIMD} ; \
	done
ifeq (i386,$(DEB_HOST_ARCH))
	set -e ; for SIMD in $(I386_SIMDE) ; do \
		export CXXFLAGS="$(CXXFLAGS) -m$${SIMD}" && export CFLAGS="$(CFLAGS) -m$${SIMD}" && \
		dh_auto_configure --builddirectory=obj-$${SIMD} ; \
	done
endif
endif
	dh_auto_configure

override_dh_auto_build-arch:
ifneq (,$(filter $(DEB_HOST_ARCH),amd64 i386))
	set -e ; for SIMD in $(AMD64_SIMDE) ; do \
		dh_auto_build --builddirectory=obj-$${SIMD} ; \
	done
ifeq (i386,$(DEB_HOST_ARCH))
	set -e ; for SIMD in $(I386_SIMDE) ; do \
		dh_auto_build --builddirectory=obj-$${SIMD} ; \
	done
endif
endif
	dh_auto_build


override_dh_install-arch:
	dh_install -a
	# install binary
	cp -a $(BUILD_DIR)/bin/*/plast debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/bin/
ifneq (,$(filter $(DEB_HOST_ARCH),amd64 i386))
	set -e ; for SIMD in $(AMD64_SIMDE) ; do \
		cp -a obj-$${SIMD}/bin/*/plast debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/bin/plast-$${SIMD} ; \
	done
ifeq (i386,$(DEB_HOST_ARCH))
	set -e ; for SIMD in $(I386_SIMDE) ; do \
		cp -a obj-$${SIMD}/bin/*/plast debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/bin/plast-$${SIMD} ; \
	done
endif
endif
	# no need for the includes
	rm -rf debian/*/usr/include

override_dh_auto_configure-indep:
override_dh_auto_build-indep:
override_dh_auto_install-indep:
	echo "I: Nothing to do for arch:indep binary"

override_dh_gencontrol-arch:
	dh_gencontrol -- -Vsimde:Built-Using="$(shell dpkg-query -f '$${source:Package} (= $${source:Version}), ' -W "libsimde-dev")"

override_dh_auto_test-indep:
override_dh_auto_test-arch:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
ifneq (,$(filter $(DEB_HOST_ARCH),amd64 i386))
	set -e ; for SIMD in $(AMD64_SIMDE) ; do \
		if lscpu | grep -q $${SIMD} ; then \
		scripts/test-plast.sh obj-$${SIMD} ; fi ; \
	done
ifeq (i386,$(DEB_HOST_ARCH))
	set -e ; for SIMD in $(I386_SIMDE) ; do \
		if lscpu | grep -q $${SIMD} ; then \
		scripts/test-plast.sh obj-$${SIMD} ; fi ; \
	done
endif
endif
	scripts/test-plast.sh
endif
