#!/bin/bash
# This is an adapted wrapper script from original
#    srcipts/plast.sh
# by adapting the launch() procedure to /usr/lib/plast/bin/plast
# For changes make sure you create a diff
#

#/*****************************************************************************
# *                                                                           *
# *   PLAST : Parallel Local Alignment Search Tool                            *
# *   Copyright (c) 2009-2015 Inria                                           *
# *                                                                           *
# *   PLAST is free software; you can redistribute it and/or modify it under  *
# *   the Affero GPL v3 License                                               *
# *                                                                           *
# *   This program is distributed in the hope that it will be useful,         *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
# *   CECILL version 2 License for more details.                              *
# *****************************************************************************/

DIR=/usr/lib/plast/bin
EXE=${DIR}/$(basename "$0")
REALPLAST=

test_and_run () {
        if lscpu | grep -q "$1" && [ -x "${EXE}-$1" ]; then
                echo "Using ${EXE}-$1" 1>&2
                REALPLAST="${EXE}-$1"
                return
        fi
}


for SIMD in sse3 sse2 ; do
	if [ -z ${REALPLAST} ] ; then
		test_and_run ${SIMD}
	fi
done

if [ -z ${REALPLAST} ] ; then
	# fallback to plain option
	echo "Using $EXE without SIMD support" 1>&2
	REALPLAST=/usr/lib/plast/bin/plast
fi

usage () {
	$REALPLAST -h
}

if [ $# -lt 1 ] ; then
	usage
	echo "$0: Missing required arguments ... exiting."
	exit 1
fi

# start plast execution: you can adapt _scripts_dir as needed if you install
# plast binary elsewhere than build/bin
launch() {
	#_scripts_dir=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
	$REALPLAST $KL_PARAMS
}

choose_query() {
	breakline
	echo "Enter the path to your query file (use [tab] key for completion): "
	read -e -p ">" query

	if [ -z $query ]; then
   		echo "/!\ You did not specify a file."
   		choose_query
	elif [ -e $query ]; then
		choose_bank
	else
   		echo "/!\ Query does not exist."
   		choose_query
	fi
}

choose_bank() {
	breakline
	echo "What is the type of your reference databank ? "
	echo "1. FASTA file"
	echo "2. Blast databank"
	read -n 1 -p "Your choice: " db_type
	echo

	case $db_type in
		1) choose_bank_fasta ;;
		2) choose_bank_blast ;;
		*) echo "/!\ Invalid choice."
		   choose_bank ;;
	esac
}

choose_bank_fasta() {
	breakline
	echo "Enter the path to the FASTA file (use [tab] key for completion): "
	read -e -p ">" bank
	if [ -z $bank ]; then
   		echo "/!\ You did not specify a file."
   		choose_bank_fasta
	elif [ -e $bank ]; then
		choose_program
	else
   		echo "/!\ Fasta file does not exist."
   		choose_bank_fasta
	fi
}

choose_bank_blast() {
	breakline
	echo "Enter the path to the Blast databank file (.pin or .pal or .nin or .nal): "
	read -e -p ">" bank
	if [ -z $bank ]; then
   		echo "/!\ You did not specify a file."
   		choose_bank_blast
	elif  [ -e $bank ]; then
		choose_program
	else
   		echo "/!\ Blast databank file does not exist."
   		choose_bank_blast
	fi
}

choose_program() {
	breakline
	echo "Choose your sequence comparison method: "
	echo "1. PLASTp: Scan a protein database with a protein query"
	echo "2. PLASTn: Scan a nucleic database with a nucleic query"
	echo "3. PLASTx: Scan a protein database with a translated nucleic query"
	echo "4. tPLASTn: Scan a translated nucleic database with a protein query"
	echo "5. tPLASTx: Scan a translated nucleic database with a translated nucleic query"
	read -n 1 -p "Your choice: " choice
	echo

	case $choice in
		1) program="plastp"
		   choose_mhpq ;;
		2) program="plastn"
		   choose_plastn_mode ;;
		3) program="plastx"
		   choose_mhpq ;;
		4) program="tplastn"
		   choose_mhpq ;;
		5) program="tplastx"
		   choose_mhpq ;;
		*) echo "/!\ Invalid choice."
		   choose_program ;;
	esac
}

choose_plastn_mode() {
	breakline
	read -p "Are you comparing close homologues ? (y/n) " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		def_evalue='1e-100'
		specials_args='-s 127 -xdrop-ungap 0 -Z 0 -max-database-size 5000000'
	fi
	choose_mhpq
}

choose_mhpq() {
	breakline
	read -p "Maximum hits per query ? Value 0 will dump all hits (10 by default): " mhpq
	mhpq=${mhpq:-10}

	if echo $mhpq | egrep -q '^[0-9]+$'; then
		choose_evalue
	else
   		echo "/!\ Wrong value."
   		choose_mhpq
	fi
}

choose_evalue() {
	breakline
	read -p "E-value threshold ? (default is $def_evalue): " evalue
	evalue=${evalue:-$def_evalue}

	if echo $evalue | egrep -q '^[+-]?((([0-9]+(\.[0-9]*)?)|(\.[0-9]+))([eE][+-]?[0-9]+)?)$'; then
		choose_out
	else
   		echo "/!\ Wrong value."
   		choose_evalue
	fi
}

choose_out() {
	breakline
	read -p "PLAST result file name (out.csv by default): " out
	out=${out:-"out.csv"}

	if [ -e $out ]
	then
		read -p "/!\ File already exists. Overwrite it ? (y/n) " -n 1 -r
		echo
		if [[ $REPLY =~ ^[Yy]$ ]]; then
			choose_outfmt
		else
			choose_out
		fi
	else
   		choose_outfmt
	fi
}

choose_outfmt() {
	breakline
	echo "Choose result format: "
	echo "1. tabular Blast-like"
	echo "2. extended tabular format"
	echo "3. XML Blast-like"
	read -n 1 -p "Your choice: " choice
	echo

	case $choice in
		1) out_fmt="1"
		   validate ;;
		2) out_fmt="2"
		   validate ;;
		3) out_fmt="4"
		   validate ;;
		*) echo "/!\ Invalid choice."
		   choose_outfmt ;;
	esac
}


validate() {
	breakline
	KL_PARAMS='-p '$program' -i '$query' -d '$bank' -max-hit-per-query '$mhpq' -e '$evalue' -o '$out
    KL_PARAMS=$KL_PARAMS' -outfmt '$out_fmt' '$specials_args' -force-query-order 1000 -stats '$out'.stats'
	KL_PARAMS=$KL_PARAMS' -bargraph '
	
	echo "PLAST arguments are: "$KL_PARAMS
	echo
	echo "IMPORTANT: ensure your terminal can display at least 150 chars/line to display PLAST progress bar."
	echo
	read -p "Ready to start PLAST ? (y/n) " -n 1 -r
	echo
	breakline
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		launch
	elif [[ $REPLY =~ ^[Nn]$ ]]; then
		helper
	else
		validate
	fi
}

breakline() {
	echo
	echo "_____________________"
}

helper() {
	# init default values
	def_evalue='1e-3'
	specials_args=''

	choose_query
}

if [ $# -eq 0 ]; then
	helper
else
	KL_PARAMS=$*
	launch
fi
