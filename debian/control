Source: plast
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               libpcre2-dev,
               default-jdk,
               libsimde-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/plast
Vcs-Git: https://salsa.debian.org/med-team/plast.git
Homepage: https://plast.inria.fr/
Rules-Requires-Root: no

Package: plast
Architecture: any
# Since the license of this package is AGPL, adding a Built-Using
Built-Using: ${simde:Built-Using}
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${java:Depends}
Description: Parallel Local Sequence Alignment Search Tool
 PLAST is a fast, accurate and NGS scalable bank-to-bank sequence
 similarity search tool providing significant accelerations of seeds-
 based heuristic comparison methods, such as the Blast suite of
 algorithms.
 .
 Relying on unique software architecture, PLAST takes full advantage of
 recent multi-core personal computers without requiring any additional
 hardware devices.

Package: plast-example
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Parallel Local Sequence Alignment Search Tool (example data)
 PLAST is a fast, accurate and NGS scalable bank-to-bank sequence
 similarity search tool providing significant accelerations of seeds-
 based heuristic comparison methods, such as the Blast suite of
 algorithms.
 .
 Relying on unique software architecture, PLAST takes full advantage of
 recent multi-core personal computers without requiring any additional
 hardware devices.
 .
 This package contains some example data to test plast.
